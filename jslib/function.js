function check_login(){
    var acc = JSON.parse(localStorage.getItem("acc"));
    var name = document.getElementById("user_name").value;
    var pass = document.getElementById("password").value;
    
    var accObj = acc.find(x => x.userName == name && x.password == pass);
    
    var id = accObj.id;                   
    localStorage.setItem("selectedAccId", id);

    if(accObj.index == 0){
        alert("The user is logging in for the first time! \nSo you will need to reset your password.");
        window.location.href = "resetpw.html";
    }else{
        if (accObj != undefined) {
            alert(name + ", Welcome to Hong Kong Kau Kau Library.");
            window.location.href = "contact.html";
        }else {
            alert("The account does not exist! \nPlease enter a valid account.");
            return;
        }
    }

    
}

function check_email(){
    var acc = JSON.parse(localStorage.getItem("acc"));
    var email = document.getElementById("email").value;

    var accObj = acc.find(x => x.email == email);

    if (accObj != undefined) {
        alert("Valid email! \nplease reset your password.");
        window.location.href = "resetpw.html";
    }else {
        alert("This email does not exist! \nPlease enter a valid email.");
        return;
    }
}

function check_pwd(){
    var acc = JSON.parse(localStorage.getItem("acc"));
    var selectedAccId = localStorage.getItem("selectedAccId");
    var nPwd = document.getElementById("nPwd").value;
    var conPwd = document.getElementById("conPwd").value; 

    var accObj = acc.find(x => x.id == selectedAccId);

    if(nPwd == conPwd){
        accObj.index = 1;
        accObj.password = nPwd;
        localStorage.setItem("acc", JSON.stringify(acc));
        window.location.href = "login_new.html";
    }else if (nPwd != conPwd) {
        alert("Please provide same a password and Confirm Password.");
        return;
    }
}

