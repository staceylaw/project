//Human Computer Interaction & GUI Programming
    
    var acc = [
        {
            "id":1,
            "userName": "Polly",
            "password": "000000",
            "email": "Polly@gmail.com",
            "post": "teaching staff",
            "index": 0 ,
        },
        {
            "id":2,
            "userName": "Stacey",
            "password": "111111",
            "email": "stacey@gmail.com",
            "post": "non-teaching staff",
            "index": 0 ,
        },
        {
            "id":3,
            "userName": "Suki",
            "password": "222222",
            "email": "suki@gmail.com",
            "post": "students",
            "index": 0 ,
        },
        {
            "id":4,
            "userName": "Daisy",
            "password": "33333333",
            "email": "Daisy@gmail.com",
            "post": "alumni",
            "index": 0 ,
        }
    ];
        if (localStorage.getItem("acc") == null)
            localStorage.setItem("acc", JSON.stringify(acc));

    var book = [];
        if (localStorage.getItem("book") == null)
            localStorage.setItem("book", JSON.stringify(book));   
    
    var magazines = [];
        if (localStorage.getItem("magazines") == null)
            localStorage.setItem("magazines", JSON.stringify(magazines)); 

    var software = [];        
        if (localStorage.getItem("software") == null)
            localStorage.setItem("software", JSON.stringify(software)); 
