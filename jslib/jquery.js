
 
$(document).ready(function(){
    //Login
    $("#login").click(function(){           
        if(!document.getElementById("user_name").value.length &&
           !document.getElementById("password").value.length){
            alert("Please provide a User Name and Password!");
            return;
        } else if(!document.getElementById("user_name").value.length){
            alert("Please provide a User Name!");
            return;
        } else if(!document.getElementById("password").value.length){
            alert("Please provide a Password!");
            return;
        }   
        check_login();
        return false;
    });

    //forgot pw
    $("#forgotpw").click(function(){
        var x = document.getElementById("email").value;
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");

        if (!document.getElementById("email").value.length){
            alert("Please provide a Email !");
            return;
        } else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length){
            alert("Please provide a valid email !");
            return ;
        }
        check_email();
        return false;
    });

    $("#resetPw").click(function(){
        if (!document.getElementById("nPwd").value.length && 
            !document.getElementById("conPwd").value.length){
            alert("Please provide a New Password and a Confirm Password !");
            return;
        } else if (!document.getElementById("conPwd").value.length){
            alert("Please provide a Confirm Password !");
            return;
        }else if(!document.getElementById("nPwd").value.length){
            alert("Please provide a New Password !");
            return;
        }
        check_pwd();
        return false;
    });

});






